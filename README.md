# La Gazette
## Homebrew news podcast with Amazon Polly

I wanted to be able to **listen** to some news website instead of reading them, so I started this little project . 
I plan to run serverless using [AWS Lambda](https://aws.amazon.com/lambda/) and [DynamoDB](https://aws.amazon.com/dynamodb/), but for now the implementation still runs on a server ( I know ..so 2016 ..) .

It goes like this :
- Scrapes the news website
- Stores the articles in a sqlite3 db
- Retrieves the news for the day
- Sends the news formated in ssml for [Amazon Polly](https://aws.amazon.com/polly/) ( and the Polly to S3)
- Generates a podcast feed
- Upload the podcast feed to a public S3 bucket

*Et voilà*, grab the podcast.xml url and then subscribe with your prefered podcast app ! 
