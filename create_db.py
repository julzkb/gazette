import sqlite3

con = sqlite3.connect('articles.db')

with con:
    cur = con.cursor()
    cur.execute("CREATE TABLE articles(date TEXT, title TEXT, subtitle TEXT, content TEXT, source TEXT, subsection TEXT)")