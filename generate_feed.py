import boto3
import logging
from feedgen.feed import FeedGenerator
from botocore.exceptions import ClientError

BUCKET_URL = 'https://la-gazette.s3-ap-southeast-2.amazonaws.com/'


def generate_feed(file_list, podcast_filename):
    '''
    Generates and update the podcast feed
    Inputs : file list from S3
    Returns: None
    '''
    fg = FeedGenerator()
    fg.load_extension('podcast')
    fg.podcast.itunes_category('News')
    fg.title('La Gazette')
    fg.description(
        "La Gazette lit quotidiennement les nouvelles nationales et internationales."
    )
    fg.logo(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTCzPMK972w-IFZpKrRIVls2AuGdsPBv0PhT_bCh_tmINtRcV1P'
    )
    fg.link(href=BUCKET_URL + 'podcast.xml')

    for f in file_list:
        fe = fg.add_entry()
        fe.id(BUCKET_URL + f)
        date = f.split('rt_france-')[1].split('.')[0]
        fe.title(f"Nouvelles du {date}")
        fe.enclosure(BUCKET_URL + f, 0, 'audio/mpeg')

    fg.rss_str(pretty=True)
    fg.rss_file(podcast_filename)


def get_files_from_s3(bucket):
    '''
    Grabs a file list from s3
    Input : bucket name
    Returns : list of files
    '''
    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket(bucket)

    results = []
    for my_bucket_object in my_bucket.objects.all():
        if ".mp3" in my_bucket_object.key:
            results.append(my_bucket_object.key)

    return results


def upload_feed_to_s3(bucket):
    '''
    Uploads the podcast feed to an S3 bucket with the right permissions
    Input : bucket
    Returns : s3 response
    '''
    s3_client = boto3.client('s3')

    try:
        response = s3_client.upload_file('podcast.xml',
                                         bucket,
                                         'podcast7.xml',
                                         ExtraArgs={
                                             'ContentType': 'text/xml',
                                             'ACL': 'public-read'
                                         })
    except ClientError as e:
        logging.error(e)
        return False

    return True


if __name__ == "__main__":

    generate_feed(get_files_from_s3('la-gazette'), 'podcast.xml')
    upload_feed_to_s3('la-gazette')