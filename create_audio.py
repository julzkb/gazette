import requests
import boto3
import json
import db_util
from datetime import date


def save_file_to_s3(bucket, filename, data):
    '''
    We save the file to Amazon S3 with the right permissions
    Inputs  : bucket: Amazon bucket name, filename: desired filename, data: parsed data
    Returns: amazon S3 return code
    '''
    s3 = boto3.resource('s3')
    obj = s3.Object(bucket, filename)

    return obj.put(Body=data, ContentType='text/html')


def send_to_polly(bucket, prefix, data):
    '''
    Text to speech , we save to S3
    Inputs : bucket = bucket name
            prefix = file prefix on s3
            data = data string to encode
    Returns : TaskID
    '''
    polly = boto3.client('polly')
    response = polly.start_speech_synthesis_task(VoiceId='Lea',
                                                 OutputS3BucketName=bucket,
                                                 Engine='standard',
                                                 LanguageCode='fr-FR',
                                                 OutputFormat='mp3',
                                                 SampleRate='24000',
                                                 OutputS3KeyPrefix=prefix,
                                                 TextType='ssml',
                                                 Text=data)

    taskId = response['SynthesisTask']['TaskId']
    return "TaskId is {}".format(taskId)


def cheap_locale(d):
    '''
    Quick hacky date translation to avoid installing French locale on Lambda
    Inputs : d : date in plain english
    Return : data in french
    '''
    weekday = {
        'Monday': 'Lundi',
        'Tuesday': 'Mardi',
        'Wednesday': 'Mercredi',
        'Thursday': 'Jeudi',
        'Friday': 'Vendredi',
        'Saturday': 'Samedi',
        'Sunday': 'Dimanche'
    }

    month = {
        'January': 'Janvier',
        'February': 'Fevrier',
        'March': 'Mars',
        'April': 'Avril',
        'May': 'Mai',
        'June': 'Juin',
        'July': 'Juillet',
        'August': 'Aout',
        'September': 'Septembre',
        'October': 'Octobre',
        'November': 'Novembre',
        'December': 'Decembre'
    }

    for k in weekday.keys():
        if k in d:
            d2 = d.replace(k, weekday[k])

    for m in month.keys():
        if m in d2:
            return d2.replace(m, month[m])


def main():

    today = date.today()
    en_today = today.strftime('%A %d %B %Y')
    today_title = today.strftime('%d-%m-%Y')

    # All of our static text formatting for SSML goes in this dict
    template = {
        'header_national':
        '<break time="1s"></break>Voici les nouvelles nationales.<break time="2s"></break>',
        'header_international':
        'Passons a présent aux nouvelles internationales.<break time="2s"></break>',
        'au revoir':
        '<break time="2s"></break>' +
        "C'est tout pour aujourdh'hui, au revoir et a demain " +
        '<break time="2s"></break>',
        'break':
        '<break time="1s"></break>'
    }

    # Grabs the actual news from the DB
    data_national = '<break time="2s"></break>{}{}'.format(
        template['header_national'],
        db_util.get_todays_articles()['national'])
    data_international = '<break time="2s"></break>{}{}'.format(
        template['header_international'],
        db_util.get_todays_articles()['international'])

    # long Fstring data to send to Pollu
    data = f"<speak><amazon:auto-breaths>{cheap_locale(en_today)}{data_national}{data_international}{template['au revoir']}</amazon:auto-breaths></speak>"
    response_polly = send_to_polly('la-gazette', f"rt_france-{today_title}",
                                   data)

    return response_polly


if __name__ == "__main__":
    print(main())