import unittest
import os
import generate_feed
import news_rt


class TestGenerateFeed(unittest.TestCase):
    def test_generate_feed(self):
        test_file = 'test_podcast.xml'
        file_list = ['rt_france-something1.mp3', 'rt_france-something3.mp3']
        generate_feed.generate_feed(file_list, test_file)

        with open(test_file, 'r') as f:
            x = len(f.read())

        os.remove(test_file)
        self.assertGreater(x, 10)

    # we need local creds for those 2 tests
    # def test_get_files_from_s3(self):
    #     x = generate_feed.get_files_from_s3('la-gazette')
    #     self.assertGreater(len(x), 0)

    # def test_upload_feed_to_s3(self):
    #     x = generate_feed.upload_feed_to_s3('la-gazette')
    #     self.assertTrue(x)


class TestNewsRt(unittest.TestCase):
    def test_get_titles_rt_national(self):
        x = news_rt.get_titles_rt('https://francais.rt.com/france')
        self.assertTrue(x)

    def test_get_titles_rt_international(self):
        x = news_rt.get_titles_rt('https://francais.rt.com/international')
        self.assertTrue(x)

    def test_send_to_db(self):
        articles = ["oh, hi there, unittest", "how are you today ?"]
        x = news_rt.send_to_db(articles, "national")
        self.assertTrue(x)

    def test_filter_articles1(self):
        new_articles = ["I am a new article", "I really should live in a db"]
        db_articles = [["2008-12-12", "I am a new article"],
                       ["2008-12-12", "I really should live in a db"]]
        x = news_rt.filter_articles(new_articles, db_articles)
        self.assertFalse(x)

    def test_filter_articles2(self):
        new_articles = [
            "I am a new article, kinda", "I really should live in a db"
        ]
        db_articles = [["2008-12-12", "I am a new article"],
                       ["2008-12-12", "I really should live in a db"]]
        x = news_rt.filter_articles(new_articles, db_articles)
        self.assertTrue(x)


if __name__ == "__main__":
    unittest.main()
