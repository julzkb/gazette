import requests
from lxml import html
import datetime
from datetime import date
import db_util

urls = {
    'international': "https://francais.rt.com/international",
    'national': "https://francais.rt.com/france"
}


def get_titles_rt(url):
    '''
    Getting titles given an RT webpage
    Input : url
    Return : list of titles
    '''
    results = []
    page = requests.get(url)
    tree = html.fromstring(page.content)
    titles = tree.xpath(
        "/html/body/div[1]/div[1]/div[6]/div/div/div[1]/div/div/div/ul/li[*]/div/div[2]/div/a/text()"
    )

    for i in titles:
        results.append(i.strip() + '.')

    return results


def send_to_db(articles, subsection):
    '''
    Generates a tuple to be inserted in the DB

    '''
    to_send = []
    for a in articles:
        to_send.append(
            (datetime.datetime.now(), a, None, None, "rt france", subsection))

    return to_send


def filter_articles(new_articles, db_data):
    '''
    Compare new articles to check if they are already in the DB
    Inputs : new articles, db_articles
    Returns : list of filtered articles
    '''
    results, db_articles = [], []
    for d in db_data:
        db_articles.append(d[1])

    for i in new_articles:
        if i in db_articles:
            pass

        else:
            results.append(i)

    return results


def main():

    for i in urls.keys():
        # get international titles
        titles = get_titles_rt(urls[i])
        # check against db
        filtered_articles = filter_articles(titles,
                                            db_util.get_articles("rt france"))
        # ingest new articles
        db_util.ingest(send_to_db(filtered_articles, i))


if __name__ == "__main__":
    main()
