import sqlite3
import datetime


def ingest(data):
    '''
    Ingest new the articles
    Input : Data formated to match out SQL query
    Returns : None

    '''
    con = sqlite3.connect('articles.db')
    with con:
        cur = con.cursor()
        cur.executemany("INSERT INTO articles VALUES(?, ?, ?, ?, ?, ?)", data)


def get_articles(source):
    '''
    Gets all the articles from the db
    Input : news source
    Returns: list of all the articles
    '''
    con = sqlite3.connect('articles.db')
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM articles WHERE source='{}'".format(source))
        rows = cur.fetchall()

    return rows


def get_todays_articles():
    '''
    Gets todays articles
    Returns : a list of todays articles ( national, international)
    '''

    results = {}
    con = sqlite3.connect('articles.db')
    with con:
        cur = con.cursor()

        for i in ['national', 'international']:
            
            cur.execute(
                "SELECT * from articles WHERE date > datetime('now', '-1 days') AND subsection='{}'".format(i))
            rows = cur.fetchall()

            titles = []
            for r in rows:
                titles.append(r[1])

            results[i] = '\n'.join(titles)

    return results
